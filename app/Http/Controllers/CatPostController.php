<?php

namespace App\Http\Controllers;

use App\CatPost;
use Illuminate\Http\Request;

class CatPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CatPost  $catPost
     * @return \Illuminate\Http\Response
     */
    public function show(CatPost $catPost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CatPost  $catPost
     * @return \Illuminate\Http\Response
     */
    public function edit(CatPost $catPost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CatPost  $catPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CatPost $catPost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CatPost  $catPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(CatPost $catPost)
    {
        //
    }
}
